<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> </title>
    </head>
    <body>
        <?php

            $total = sum(2,3);
            echo($total . " ");
            $total = sum(2,3,4);
            echo($total . " ");
            $total = sum(2,3,4,5);
            echo($total);

            function sum(...$values) {

                $result = 0;

                foreach($values as $x)
                {
                    $result += $x;
                }

                return $result;
            }

        ?>
    </body>
</html>