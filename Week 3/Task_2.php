<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> </title>
    </head>
    <body>
        <?php

            $filterEvenNum = fn(array $arr) : array => array_map(fn($val) => ($val%2==0) ? $val : null, $arr);

            $result = $filterEvenNum(array(2,3,4,6,7,9,11,20));

            foreach($result as $x)
            {
                echo($x . " ");
            }
        ?>
    </body>
</html>