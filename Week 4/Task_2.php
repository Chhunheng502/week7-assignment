<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> </title>
    </head>
    <body>
        <?php

            require_once 'library/Payment.php';
            require_once 'library/ABA.php';
            require_once 'library/Wing.php';
            require_once 'library/PiPay.php';

            require_once 'library/ProductSale.php';

            $myData = new ProductSale();

            $myData->add("Item 1", 5, 1, "ABA");
            $myData->add("Item 2", 3, 2, "Wing");
            $myData->add("Item 3", 4, 1, "ABA");
            $myData->add("Item 4", 5, 1, "ABA");
            $myData->add("Item 5", 6, 1, "PiPay");
            $myData->add("Item 6", 10, 1, "ABA");
            $myData->add("Item 7", 15, 1, "Wing");
            $myData->add("Item 8", 2, 1, "Wing");

            $myData->getTotalSale_ABA();
            echo "<br>";
            $myData->getTotalSale_Wing();
            echo "<br>";
            $myData->getTotalSale_PiPay();
            echo "<br>";
            $myData->getTotalSale();

        ?>
    </body>
</html>