
<!--

Explain the advantage and disadvantage of multiple inheritance.

_Advantages:

.Classess can be categorized in many different ways, having some attributes or methods in common.

.Subclass can reuse attributes and methods of multiple superclasses. This will shorten the code and make it more organized.

_Disadvantages:

.The more superclassess that subclass inherits from, the more complexity it will be. This is a matter of maintenance.
If there is a change in superclasses, the subclass will have to change as well.

.If two superclasses have a method with the same name, the subclass won't know which one to call.

-->


<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> </title>
    </head>
    <body>
        <?php

            trait Job {

                private $job;

                public function getJob() {

                    if($this->job==null)
                    {
                        $this->job = "homeless";
                    }

                    return $this->job;
                }
            }

            trait Income {

                private $income;

                public function getIncome() {

                    if($this->income==null)
                    {
                        $this->income = 0;
                    }

                    return $this->income . "$";
                }
            }

            trait Nationality {

                public function getNationality() {

                    return "Cambodian";
                }
            }

            class Person {

                use Job;
                use Income;
                use Nationality;

                private $name;
                private $age;

                public function __construct($name, $age) {

                    $this->name = $name;
                    $this->age = $age;
                }

                public function changeJob($job) {

                    $this->job = $job;
                }

                public function changeIncome($income) {

                    $this->income = $income;
                }

                public function getInfo() {

                    echo "Name: " . $this->name;
                    echo "<br>";
                    echo "Age: " . $this->age;
                    echo "<br>";
                    echo "Job: " . $this->getJob();
                    echo "<br>";
                    echo "Income: " . $this->getIncome();
                    echo "<br>";
                    echo "Nationality: " . $this->getNationality();
                }
            }

            $person = new Person("Chhunheng Leng", 21);

            $person->changeIncome(300);

            $person->getInfo();

        ?>
    </body>
</html>

