<?php

    require_once 'Payment.php';

    class Wing extends Payment {

        public function __construct($productName, $price, $quantity) {

            $this->productName = $productName;
            $this->price = $price;
            $this->quantity = $quantity;
        }

        public function getTotalSale() {

            return $this->price * $this->quantity;
        }
    }

?>