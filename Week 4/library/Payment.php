<?php

    abstract class Payment {

        private string $productName;
        private float $price;
        private int $quantity;

        abstract public function getTotalSale();

    }

?>