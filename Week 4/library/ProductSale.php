<?php

    require_once 'Payment.php';
    require_once 'ABA.php';
    require_once 'Wing.php';
    require_once 'PiPay.php';

    class ProductSale extends Payment {

        private $product_List;

        public function __construct() {

            $this->product_List = array();
        }

        public function add($newName, $newPrice, $newQuantity, $method)
        {
            if($method == "ABA")
            {
                $item = new ABA($newName, $newPrice, $newQuantity);
                array_push($this->product_List, $item);
            }
            elseif($method == "Wing")
            {
                $item = new Wing($newName, $newPrice, $newQuantity);
                array_push($this->product_List, $item);
            }
            elseif($method = "PiPay")
            {
                $item = new PiPay($newName, $newPrice, $newQuantity);
                array_push($this->product_List, $item);
            }
            
        }

        public function getTotalSale_ABA() {

            echo "Number of sales by ABA method: ";

            foreach($this->product_List as $x)
            {
                if(is_a($x, "ABA"))
                {
                    echo $x->getTotalSale() . " ";
                }
            }
        }

        public function getTotalSale_Wing() {

            echo "Number of sales by Wing method: ";

            foreach($this->product_List as $x)
            {
                if(is_a($x, "Wing"))
                {
                    echo $x->getTotalSale() . " ";
                }
            }
        }

        public function getTotalSale_PiPay() {

            echo "Number of sales by PiPay method: ";

            foreach($this->product_List as $x)
            {
                if(is_a($x, "PiPay"))
                {
                    echo $x->getTotalSale() . " ";
                }
            }
        }

        public function getTotalSale() {

            for($i = 0; $i < sizeof($this->product_List)-1; $i++)
            {
                $max = $this->product_List[$i]->getTotalSale();
                $index = $i;

                for($j = $i + 1; $j < sizeof($this->product_List); $j++)
                {
                    if($max < $this->product_List[$j]->getTotalSale())
                    {
                        $max = $this->product_List[$j]->getTotalSale();
                        $index = $j;
                    }
                }

                if($index != $i)
                {
                    $temp = $this->product_List[$i];
                    $this->product_List[$i] = $this->product_List[$index];
                    $this->product_List[$index] = $temp;
                }
            }

            echo "All sales ordering by biggest total amount: ";

            foreach($this->product_List as $x)
            {
                echo $x->getTotalSale() . " ";
            }
        }


    }

?>