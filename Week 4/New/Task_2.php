<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <title> </title>
    </head>
    <body>
        <?php

            require_once 'library/Payment.php';
            require_once 'library/ABA.php';
            require_once 'library/Wing.php';
            require_once 'library/PiPay.php';

            require_once 'library/ProductSale.php';

            $myData = new ProductSale();

            $data1 = new ABA("Item 1", 5, 1);
            $data2 = new Wing("Item 2", 3, 2);
            $data3 = new ABA("Item 3", 4, 1);
            $data4 = new ABA("Item 4", 5, 1);
            $data5 = new PiPay("Item 5", 6, 1);
            $data6 = new ABA("Item 5", 6, 1);
            $data7 = new Wing("Item 7", 15, 1);
            $data8 = new Wing("Item 8", 2, 1);

            $myData->record_Data($data1,$data2,$data3,$data4,$data5,$data6,$data7,$data8);

            $ABA = new ABA("temp", 0, 0);
            $Wing = new Wing("temp", 0, 0);
            $PiPay = new PiPay("temp", 0, 0);

            echo "Number of sales by ABA method: " . $myData->getTotalSales_ByMethod($ABA);
            echo "<br>";
            echo "Number of sales by ABA and Wing methods: " . $myData->getTotalSales_ByMethod($ABA, $Wing);
            echo "<br>";
            $myData->displaySales_ByBiggest($ABA, $Wing, $PiPay);
        ?>
    </body>
</html>