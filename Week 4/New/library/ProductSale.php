<?php

    require_once 'Payment.php';
    require_once 'ABA.php';
    require_once 'Wing.php';
    require_once 'PiPay.php';

    class ProductSale {

        private $product_List;

        public function __construct() {

            $this->product_List = array();
        }

        public function record_Data(...$values)
        {
            foreach($values as $x)
            {
                array_push($this->product_List, $x);
            }
        }

        public function getTotalSales_ByMethod(...$values) {

            $totalSale = 0;

            foreach($this->product_List as $x)
            {
                foreach($values as $y)
                {
                    if(is_a($x, get_class($y)))
                    {
                        $totalSale += $x->getTotalSales();
                    }
                }
            }

            return $totalSale;
        }

        public function displaySales_ByBiggest(...$values) {

            $eachSales_array = array();

            foreach($values as $x)
            {
                array_push($eachSales_array, ProductSale::getTotalSales_ByMethod($x));
            }

            rsort($eachSales_array);

            echo "Total sales ordering from biggest amount: ";

            foreach($eachSales_array as $x)
            {
                foreach($values as $y)
                {
                    if(ProductSale::getTotalSales_ByMethod($y) == $x)
                    {
                        echo get_class($y) . '(' . $x . ')' . ' ';
                    }
                }
            }
        }
}

?>