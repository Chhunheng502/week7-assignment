<?php

    require_once 'Payment.php';

    class ABA extends Payment {

        public function __construct($productName, $price, $quantity) {

            $this->productName = $productName;
            $this->price = $price;
            $this->quantity = $quantity;
        }

        public function getTotalSales() {

            return $this->price * $this->quantity;
        }
    }

?>